/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import styles from './DrawingPage.css';
import withStyles from '../../decorators/withStyles';
import ReactDOM from 'react-dom';
import guid from './Utils';

@withStyles(styles)
class DrawingPage extends Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired
  };

  constructor() {
    super()
    this.state = {selectedColor: '#000000', rotated: false};
  }

  render() {
    let title = 'Draw';
    this.channelGuid = 'mike-demo'; //guid();
    this.context.onSetTitle(title);
    let colors = [
      '#000000', '#ff0000', '#00ff00', '#0000ff', '#ffffff'
    ];
    return (
      <div className="DrawingPage">
        <div className="DrawingPage-container">

          <div ref="drawingNode"></div>

          <div className="DrawingPage-collaborators">
            Invite Collaborators to Room {this.channelGuid}
          </div>

          <div className="DrawingPage-colorPicker">
            {
              colors.map(color => (
                <span key={color}
                      style={{backgroundColor:color}}
                      className={this.state.selectedColor == color ? 'active' : ''}
                      onClick={this.setColor.bind(this, color)}>
                </span>
              ))
            }
          </div>

        </div>
      </div>
    );
  }

  setColor(color) {
    this.setState({selectedColor: color});
    this.lc.setColor('primary', color);
  }

  componentDidMount() {
    this.pubnub = window.pubnub;
    this.userId = Math.random(); //TODO:

    // TODO: make sure we have the latest snapshot state if this is a shared room
    // TODO: implement all the join/leave semantics for collaborators. perhaps
    // the "creator" is the one who is reponsibile for giving noobs the current
    // state of the room when they join ... and if the creator leaves, everyone
    // else is kicked out.

    // tee up literallycanvas
    // see api at http://literallycanvas.com/api/initializing.html
    // TODO: consider going lower level, e.g. fabric.js
    // ideas:
    //    - selectable background (graph paper, grid, chalk board, ...)
    //    - fixed 1920x1080 canvas size, but scale via zoom for other sized viewports

    var screen = {width: window.innerWidth, height: window.innerHeight};
    var canvas = {width: 1920, height: 1080};
    var scaleX = 1.0, scaleY = 1.0;
    var rotate = 0;
    if ((screen.width / screen.height) < (canvas.width / screen.width)) {
      // portrait mode (tall) ... zoom+crop
      // Swap width & height to make it 1080x1920
      //size = {width: size.height, height: size.width};
      //this.state.rotated = true;
      rotate = 90;
      console.log('rotated! screen: ', screen, ', canvas: ', canvas);
    }
    if (screen.width != canvas.width) {
      // zoom if canvas is different than window
      //  e.g. canvas is 100px but window is only 50px.
      //    ==> zoom = 2
      //  e.g. canvas is 100px but window is 200px.
      //    ==> zoom = 0.5
      scaleX = 1 / (canvas.width / screen.width);
      console.log('zoooming based on width to ', scaleX, screen.width, canvas.width);
    }
    if (screen.height != canvas.height) {
      // keep zooming in if canvas height is larger than window
      scaleY = 1 / (canvas.height / screen.height);
      console.log('zoooming based on height to ', scaleY, screen.height, canvas.height);
    }

    var drawingNode = ReactDOM.findDOMNode(this.refs.drawingNode);
    drawingNode.style.transform = 'scale('+scaleX+','+scaleY+') rotate('+rotate+'deg)';

    this.lc = LC.init(
        drawingNode,
        {
          imageURLPrefix: '/bower_components/literallycanvas/img',
          imageSize: canvas,
          // TODO: implement canvas rotation for portrait vs. landscape
          primaryColor: this.state.selectedColor,
          secondaryColor: 'transparent',
          backgroundColor: 'transparent',
          defaultStrokeWidth: 10,
          tools: [
            LC.tools.Pencil,
            LC.tools.Eraser,
            LC.tools.Line,
            LC.tools.Rectangle,
            //LC.tools.Text,
            //LC.tools.Polygon,
            //LC.tools.Pan,
            //LC.tools.Eyedropper
          ]
        }
    );
    //console.log('zooom', zoom, 1/zoom);
    //this.lc.setZoom(zoom);

    // Capture drawing/undo/redo events
    // See events at http://literallycanvas.com/api/events.html
    // TODO sync out to all collaborators via pubnub (or other gateway)
    // http://www.pubnub.com/blog/multiuser-draw-html5-canvas-tutorial/
    this.unsubscribe = [
      this.lc.on('shapeSave', function(args) {
        console.log('shapeSave triggered', args);
        //if (!args.shape.className) {
        //  args.shape.className = 'LinePath';
        //}
        //let snapshot = this.lc.getSnapshot(['shapes']);
        this.syncMessage({_type: 'saveShape', shape: LC.shapeToJSON(args.shape), previousShapeId: args.previousShapeId});
      }.bind(this)),
      this.lc.on('undo', function() {
        console.log('undo triggered', this.lc.getSnapshot());
      }.bind(this)),
      this.lc.on('redo', function() {
        console.log('redo triggered', this.lc.getSnapshot());
      }.bind(this)),
      this.lc.on('clear', function() {
        // TODO: avoid infinte loop
        this.syncMessage({_type: 'clear'});
      }.bind(this))
    ];

    this.pubnub.subscribe({
      channel: this.channelGuid,
      callback: function(message) {
        if (message._userId == this.userId) {
          console.log('ignoring message to self', message);
          return;
        }
        console.log('got message from remote', message);
        if (message._type == 'saveShape') {
          this.lc.saveShape(LC.JSONToShape(message.shape), false, message.previousShapeId);
          //this.lc.loadSnapshot(message.snapshot);
        }
        if (message._type == 'clear') {
          // TODO: avoid infinte loop
          this.lc.clear();
        }
      }.bind(this)
    });

  }

  syncMessage(message) {
    message._userId = this.userId;
    var payloadSize = calculate_payload_size(this.channelGuid, message);
    if (payloadSize >= 32000) {
      console.warn('Attempting to send too large of a payload', payloadSize);
    }
    this.pubnub.publish({
      channel: this.channelGuid,
      message: message
    });
  }

  componentWillUnmount() {
    console.log('Unsubscribing');
    this.unsubscribe.map(fn => fn());
  }

}

function calculate_payload_size( channel, message ) {
  return encodeURIComponent(channel + JSON.stringify(message)).length + 100;
}

export default DrawingPage;
